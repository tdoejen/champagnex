import lcdlib
def invokeLcd():
    global lcd
    try:        
    # LCD-Adresse
         ADDRESS = 0x27
    # Objekt erzeugen und Display initialisieren
         lcd = lcdlib.lcd(ADDRESS,2,16)
         lcd.clear()
         return lcd
    except:
        return False
lcd = invokeLcd()

lcd.clear()
lcd.display_string("test", 0, 0)