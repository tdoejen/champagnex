from RPi import GPIO
from time import sleep

clk = 17
dt = 18

GPIO.setmode(GPIO.BCM)
GPIO.setup(clk, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(dt, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

counter = 0
clkLastState = GPIO.input(clk)

text = ["ais","temp","gps","music"]

try:
    while True:
                clkState = GPIO.input(clk)
                dtState = GPIO.input(dt)
		
                if clkState != clkLastState:
                        if dtState != clkState:
                                counter += 1
	                        if counter > 3:
                                   counter = 0
                                print text[counter]
                        else: 
				counter -= 1
	                        if counter < 0:
                                   counter = 3
                                print text[counter]
                        #print counter
                clkLastState = clkState
                sleep(0.01)
finally:
        GPIO.cleanup()
