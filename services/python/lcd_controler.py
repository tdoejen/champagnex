#!/usr/bin/python
# -*- coding: latin-1 -*-
# http://www.netzmafia.de/skripten/hardware/RasPi/Projekt-LCD/index.html
import lcdlib
from datetime import datetime
from time import sleep
from random import randint
import checki2c_devices

# Bildschirm löschen

needtoggleOn = True
def invokeLcd():
    global lcd
    try:        
    # LCD-Adresse
         ADDRESS = 0x27
    # Objekt erzeugen und Display initialisieren
         lcd = lcdlib.lcd(ADDRESS,2,16)
         lcd.clear()
         return lcd
    except:
        return False

lcd = invokeLcd()
def lcdClear():
    lcd.clear()
    return lcd

def lcdBlink():
    lcd.cursor_blink()
    return

def correctStringLength(inputStr):
    outputStr = inputStr
    if len(inputStr) < 16:
        lenstrListOld = 16 - len(inputStr)
        s = ""
        for i in range(lenstrListOld):
         outputStr+=str(" ")
    return outputStr 



def compareStrings(lcdObject):

    lcdLine0Old =  correctStringLength(lcdObject["lcd_1"]["old"])
    lcdLine0New =  correctStringLength(lcdObject["lcd_1"]["new"])
 
    # Lcd Line 1
    lcdLine1Old =  correctStringLength(lcdObject["lcd_2"]["old"])
    lcdLine1New =  correctStringLength(lcdObject["lcd_2"]["new"])
    lcdUpdate = {}
    lcdUpdate["lcd_1"] = []
    lcdUpdate["lcd_2"] = []
    for i in range(16):
        if lcdLine0Old[i] != lcdLine0New[i]:
            lcdUpdate["lcd_1"].append([lcdLine0New[i], 0, i])
        if lcdLine1Old[i] != lcdLine1New[i]:
            lcdUpdate["lcd_2"].append([lcdLine1New[i], 1, i])
    return lcdUpdate

def lcdUdate(lcdUpdateObj):
    global lcd,needtoggleOn

    if lcd != "off":
          if  lcdUpdateObj["clear"]== False:
            lcdUpdateObj = compareStrings(lcdUpdateObj)
            for rec in lcdUpdateObj["lcd_1"]:
                lcd.display_string(rec[0], rec[1], rec[2])
            for rec in lcdUpdateObj["lcd_2"]:
                lcd.display_string(rec[0], rec[1], rec[2])

          else:
            lcdClear()
            lcd.display_string(lcdUpdateObj["lcd_1"]["new"], 0, 0)
            lcd.display_string(lcdUpdateObj["lcd_2"]["new"], 1, 0)


def lcdInitalString(obj):
    try:
        lcd.clear()

        lcd.display_string(obj["lcd_1"]["initial"], 0, 0)
        lcd.display_string(obj["lcd_2"]["initial"], 1, 0)
    except:
        return "lcdNotConnected"

