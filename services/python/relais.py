import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM) # GPIO Nummern statt Board Nummern
RELAIS_1_GPIO = 26

def toggleRelais(status):
 GPIO.setup(RELAIS_1_GPIO,GPIO.OUT)

 if status == "on":
  GPIO.output(RELAIS_1_GPIO, GPIO.HIGH) # a
 if status == "off": 
  GPIO.output(RELAIS_1_GPIO, GPIO.LOW) # aus
 #GPIO.cleanup()  
 return

