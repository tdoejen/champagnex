import psycopg2
connection = None
def dbConnection():
    global connection
    try:
        connection = psycopg2.connect(user = "pi",
                                    password = "D!3W.A=champagnex",
                                    host = "127.0.0.1",
                                    port = "5432",
                                    database = "champagnex")
        return connection
    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)
        return None



def insertData(data):
   global  connection, cursor
   if connection == None:
       connection = dbConnection()
       if connection == None:
           return
       cursor = connection.cursor()    
   try:
#    print("DATA_DB", data)
    if data["gps"]["latNum"] != "no_data" and data["gps"]["longNum"] != "no_data":
     geomQeuery ="SELECT ST_SetSRID(ST_MakePoint (" + str(data["gps"]["latNum"]) + "," +  str(data["gps"]["longNum"]) + "),4326)"
     cursor.execute(geomQeuery)
     geom = cursor.fetchone()
    else:
     return
    if data["bme280"]["temperature"] != "no_data":
        temp_cabin = float(data["bme280"]["temperature"])
    else:
        temp_cabin = None
    if data["bme280"]["humidity"] != "no_data":
        humidity = float(data["bme280"]["humidity"])
    else:
        humidity = None
    if data["bme280"]["pressure"] != "no_data":
        pressure = float(data["bme280"]["pressure"])
    else:
        temp_cabin = None    

    if data["date"]["dateTime"] != "no_data":
        time = data["date"]["dateTime"]
    else:
        time = None
    postgres_insert_query = """ INSERT INTO track.data ( time ,temp_cabin, humidity, pressure, geom) VALUES (%s,%s,%s,%s, %s)"""
    record_to_insert = (time,temp_cabin, humidity, pressure, geom)
    cursor.execute(postgres_insert_query, record_to_insert)
    connection.commit()
   except (Exception, psycopg2.Error) as error :
    if(connection):
        print("Failed to insert record into mobile table:", error)




# ChampangeX-# \d track.data
#                                            Table "track.data"
#     Column    |             Type              |                        Modifiers
# --------------+-------------------------------+---------------------------------------------------------
#  id           | integer                       | not null default nextval('track.data_id_seq'::regclass)
#  time         | timestamp without time zone[] |
#  temp_cabin   | numeric                       |
#  temp_cockpit | numeric                       |
#  temp_water   | numeric                       |
#  pressure     | numeric                       |
#  humidity     | numeric                       |
#  cog          | numeric                       |
#  sog          | numeric                       |
#  geom         | geometry(Point,4326)          |
# Indexes:psql

# CREATE TABLE track.data(

#   id            serial,
#   time          timestamp with time zone ,
#   temp_cabin    numeric,                       
#   temp_cockpit  numeric ,                     
#   temp_water   numeric  ,                     
#   pressure      numeric ,                      
#   humidity      numeric  ,                     
#   cog           numeric  ,                     
# 	  sog           numeric  ,                     
#   geom          geometry(Point,4326)          

# );