
# Read DS180B20 Data
# 3v in Gipo 7 
import os
import glob
import time
 
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
#base_dir = '/sys/bus/w1/devices/'
#device_folder = glob.glob(base_dir + '28*')[0]
#device_file = device_folder + '/w1_slave'

def setDevice(i):
    try:
        base_dir = '/sys/bus/w1/devices/'
        device_folder = glob.glob(base_dir + '28*')[i]       
        device_file = device_folder + '/w1_slave'        
        return device_file
    except:
        return "noDeviceDS180Found"


def read_temp_raw(device_file):
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def readLines(lines):
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temperature = float(temp_string) / 1000.0
        return str(temperature)

 
def getDs180Data():
   try:
    OuTTempDev = setDevice(0)
    WTempDev = setDevice(1)
    data={}
    if WTempDev == "noDeviceDS180Found":
       data["WTempError"]="noDS180Found"
       data["WTemp"]= "no_data"
    else:
       lines = read_temp_raw(WTempDev)
       data["WTemp"] = readLines(lines)

    if OuTTempDev == "noDeviceDS180Found":
       data={}
       data["OuTTempError"]="noDS180Found"
       data["OutTemp"]= "no_data"
    else:
       lines = read_temp_raw(OuTTempDev)
       data["OutTemp"] = readLines(lines)
    
    return data
   except:
      data["WTemp"]= "no_data"
      data["OutTemp"] = "no_data"
      return  data
