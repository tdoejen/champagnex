import sys
import lcd_controler
import checki2c_devices
lcdDispOld1 = {}
lcdDispOld2 = {}
lcdDisp1AddOld = {}
lcdDisp2AddOld = {}
globalClear = False
import pi_system

class Subscriber:
    global lcd, globalClear
    def __init__(self, name):
        self.name = name
    # Lcd log on dipatched Data change
    def updateLcd(self, message):
        global globalClear ,lcdDispOld1,lcdDispOld2,lcdDisp1AddOld,lcdDisp2AddOld
        lcdIsOn = checki2c_devices.checkLcdPower()
        if lcdIsOn == False:
          globalClear = True
          return
        if globalClear == True:
          globalClear = False
          pi_system.restartPy()

        lcdDisp1 = message["lcd"]["lcd_1"]["disp"]
        lcdDisp2 = message["lcd"]["lcd_2"]["disp"]
        lcdDisp1Add = message["lcd"]["lcd_1"]["add"]
        lcdDisp2Add = message["lcd"]["lcd_2"]["add"]

        if lcdDisp1[0] == "mainList":
          message["lcd"]["lcd_1"]["new"] = message[lcdDisp1[0]]

        if lcdDisp1Add != "None":
         message["lcd"]["lcd_1"]["new"] = lcdDisp1Add + message[lcdDisp1[0]][lcdDisp1[1]]
        else:
         message["lcd"]["lcd_1"]["new"] = message[lcdDisp1[0]][lcdDisp1[1]]
        if lcdDisp2Add != "None":
          message["lcd"]["lcd_2"]["new"] = lcdDisp2Add + message[lcdDisp2[0]][lcdDisp2[1]]
        else:
          message["lcd"]["lcd_2"]["new"] = message[lcdDisp2[0]][lcdDisp2[1]]
        ## SET TRUE
        test = lcd_controler.lcdUdate(message["lcd"])
        
  
        if test == "lcdNotConnected":
          message["lcd"]["lcd_1"]["old"]  = "                   "
          message["lcd"]["lcd_2"]["old"]  = "                   "
        else:
          message["lcd"]["lcd_1"]["old"] =  message["lcd"]["lcd_1"]["new"]
          message["lcd"]["lcd_2"]["old"] =  message["lcd"]["lcd_2"]["new"]
        return




class Publisher:
    def __init__(self):
        self.subscribers = set()
    def register(self, who):
        self.subscribers.add(who)
    def unregister(self, who):
        self.subscribers.discard(who)
    def dispatch(self, message):
        for subscriber in self.subscribers:
         subscriber.updateLcd(message)

