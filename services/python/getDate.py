import datetime
import pytz
time = datetime.datetime.now()

def getDate():
    dateTime = datetime.datetime.now(pytz.timezone('Europe/Berlin'))
    date = "   " +  datetime.datetime.now(pytz.timezone('Europe/Berlin')).strftime("%d.%m.%Y")
    time = "     " + datetime.datetime.now(pytz.timezone('Europe/Berlin')).strftime("%H:%M") 
    return {"dateTime":dateTime,"date": date, "time": time}
