import subprocess
import signal
import os
from relais import toggleRelais 
def startAisDecoder():
    try:
        command = "rtl_ais -p 80 -T -P 8080 " #-S 1 -n
        process = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE)
        return 
    except subprocess.CalledProcessError as e:
        print (e.output)

def killAllOnAddress():
    try:
        command ="pkill -f rtl_ais"  #"sudo kill $(sudo lsof -t -i:8080) "
        process = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE, preexec_fn=os.setsid)
        process.wait()
        os.killpg(os.getpgid(process.pid), signal.SIGTERM) 
        toggleRelais("off")
        return
    except:
        return


