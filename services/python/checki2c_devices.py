#!/usr/bin/env python
import time
import smbus

bus = smbus.SMBus(1) # 1 indicates /dev/i2c-1
def checkLcdPower():
        lcdi2c = 39 #(77)
        try:
            bus.read_byte(lcdi2c)
            return True
        except: # exception if read_byte fails
            return False
# while True:
#     time.sleep(0.2)
#     print(checkLcdPower())