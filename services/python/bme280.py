import time
import board
import busio
import adafruit_bme280
i2c = busio.I2C(board.SCL, board.SDA)	
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)
def getBme280Data():

	try:

       
		data = {}
		data["temperature"]=str(bme280.temperature)
		data["humidity"]= str(bme280.humidity)
		data["pressure"] =str( bme280.pressure)
		data["error"]= None
		return data
	except:
		data={}
		data["error"] ="noBME280"
		data["temperature"] = "no_data"
		data["humidity"] =  "no_data"
		data["pressure"] = "no_data"
		return data

