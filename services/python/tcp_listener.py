# -*- coding: utf-8 -*-
from socket import *
from NmeaParser import nmeaParser

s = socket(AF_INET, SOCK_STREAM)
s.connect(('localhost', 10110)) 

def getGpsDict():
  data = s.recv(1024)
  data = data.decode("utf-8")
  arry = data.split("$")
  del arry[-1]
  data={}
  for rec in arry:
    data.update(nmeaParser(rec,{}))
  return data





 
