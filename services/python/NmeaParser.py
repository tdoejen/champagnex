# -*- coding: utf-8 -*-

def parseLat(lat,lat_dir):
    latDegres = lat[:2]
    latMin = lat[2:]
    latString =  latDegres  + chr(223) +"  " +  latMin + " " +  lat_dir + "     "
    latNum = float(latDegres) + float(latMin)/60

    return [latString, latNum]

def parseLon(lon,lon_dir):
    lonDegres = lon[:3]
    lonMin = lon[3:]
    lonNum = float(lonDegres) + float(lonMin)/60
    lonString = lonDegres + chr(223) + " " + lonMin + " "  + lon_dir + "       "

    return [lonString, lonNum]

def GPGGA(data,gpsDict):
    parsedLatString =  parseLat(data[2],data[3])
    parsedLonString = parseLon(data[4], data[5])
    gpsDict["latitude"] = parsedLatString[0]
    gpsDict["longitude"] = parsedLonString[0]
    gpsDict["latNum"] =  parsedLatString[1]
    gpsDict["longNum"] = parsedLonString[1]
    gpsDict["longitude_dir"] = data[5]
    gpsDict["fix_quality"]= data[6]
    gpsDict["number_of_sat"] = data[7]
    gpsDict["altitude"] = data[9]
    return gpsDict
def GPRMC(data,gpsDict):
    parsedLatString =  parseLat(data[3],data[4])
    parsedLonString = parseLon(data[5], data[6])
    gpsDict["latitude"] = parsedLatString[0]
    gpsDict["longitude"] = parsedLonString[0]
    gpsDict["latNum"] =  parsedLatString[1]
    gpsDict["longNum"] = parsedLonString[1]
    gpsDict["speed_knots"] = data[7]
    gpsDict["cog"] = data[8]
    return gpsDict



def GPVTG (data,gpsDict):
    # T True track made good
    # M Magnetic track made good
    artOfTrack = data[2]
    if artOfTrack == "T":
      gpsDict["cog"] = data[1]
    if artOfTrack == "M":
      gpsDict["mag_track"]= data[1]
    gpsDict["speed_knots"] = data[5]
    gpsDict["kmh"] = data[7]
    return gpsDict

def empty(data,gpsDict):
   return {"test": "empty"}


def sentence(data,gpsDict):
        split = data.split(",")
        switcher={
              "GPGGA": GPGGA,
              "GPRMC": GPRMC,
              "GPVTG": GPVTG
             }
 
        return switcher.get(split[0],empty)(split,gpsDict)



def nmeaParser(data,gpsDict):
    split = data.split(",")
    dataParsed = sentence(data,gpsDict)
    return dataParsed
