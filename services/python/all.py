# -*- coding: utf-8 -*-
from time import sleep
import RPi.GPIO as GPIO
import sys
sys.path.insert(0,"/home/pi/repos/KY040/ky040/")
import KY040
import _thread
import time

import lcd_controler

import db
# Get TcpListener gpsUpdate
import pi_system
from getDate import getDate
# Init data Listener/dispatcher
from observer1 import Publisher, Subscriber
pub = Publisher()
dataSub = Subscriber('data')
pub.register(dataSub)



data = {
"mainList":{"GPS": "GPS", "WEATHER": "WEATHER", "PI":"PI"},
'lcd':{"clear": False,'lcd_1':{"initial": "WakeUp"}, "lcd_2":{ "initial": "ChampangeX"}},
'gps':{
 'speed_knots': 'no_data',
 'fix_quality': 'no_data', 
 'longitude_dir': 'no_data', 
 'latNum': 'no_data',
 'longNum':'no_data',
 'altitude': 'no_data', 
 'longitude': 'no_data', 
 'kmh': 'no_data', 
 'latitude': 'no_data', 'cog': 'no_data',
 'number_of_sat': 'no_data'},
'bme280':{"temperature": "no_data","pressure": "no_data","humidity": "no_data"},
'ds180': {"WTemp": "no_data", "OutTemp": "no_Data"},
'ais':{'status':'off', 'toggle':'on'},
"None": {"None": "                 "},
"pi":{"reboot":"reboot","shutdown": "shutdown", "restart_py":"restart_py","return":"return"},
"date": {"dateTime":"no_data","date": "no_data", "time": "no_data"}
}

lcd_controler.lcdInitalString(data["lcd"])
# Initial Display Main List
data['lcd'] = {"clear": False,'lcd_1':{"old": "WakeUp", "disp":["mainList", "GPS"], "add": "None"}, "lcd_2":{ "old": "ChampangeX","disp":["None", "None"],"add": "None"}}
pub.dispatch(data)


# blckListener
blockListener = True

def updateData():
 global data, blockListener
 while True:
  sleep(1)

  # Update wich Data check Gps,Weater..
#   getNewDate = getDate()
#   getNewGpsData = getGpsDict()
#   getNewBme280Data = getBme280Data()
#   getNewDs180Data = getDs180Data() 
 # t = getDataFromSignalk()
#   data["date"].update(getNewDate)
#   data["gps"].update(getNewGpsData)
#   data["ds180"].update(getNewDs180Data)
#   data["bme280"].update(getNewBme280Data)
  data["lcd"]["clear"] = False
  if blockListener == False:
   pub.dispatch(data)

# Start new Thread (getting data from sensor)

import requests

def getDataFromSignalk():
   r = requests.get('http://localhost:3000/signalk/v1/api/')
   print(r.json())
   return r.json()


def storeData():
   global data
   while True:
      sleep(5)
      db.insertData(data)


_thread.start_new_thread( updateData, ( ) )
_thread.start_new_thread( storeData, ( ) )
# MainList and Initial arryStatus
maxLen=4

arryStatus = 0
selected = "MAINSELECTION"
rotary = "true"

# ArryCounter from Kyo 
def arryCounter(direction):
    
    global arryStatus,register, unregister,  maxLen, blockListener   
    if direction == 0:
       arryStatus += 1
       if arryStatus > maxLen:
          arryStatus = 0
          register = 0          
    else:
       arryStatus -= 1
       if arryStatus < 0:
          arryStatus = maxLen
    return arryStatus


# Listener on Rotary
def rotaryChange(direction): 
    print("rot")
    global rotary,arry,selected, maxLen, blockListener,data
    blockListener = True
    data["lcd"]["clear"] = True
    if rotary == "true":
     arryStatus = arryCounter(direction)
     if selected == "GPS":
       if arryStatus == 0:
        data['lcd']['lcd_1'].update({"disp":["gps","latitude"], "add": "None"})
        data['lcd']['lcd_2'].update({"disp":["gps","longitude"], "add": "None"})

       if arryStatus == 1:
        data['lcd']['lcd_1'].update({"disp":["gps","cog"], "add": "Cog:"})
        data['lcd']['lcd_2'].update({"disp":["gps","speed_knots"], "add": "Speed:"})

       if arryStatus == 2:
        data['lcd']['lcd_1'].update({ "disp":["gps","number_of_sat"], "add": "number_of_sat:"})
        data['lcd']['lcd_2'].update({ "disp":["gps","fix_quality"], "add": "fix_quality:"})

       pub.dispatch(data)
       blockListener = False
       return
     if selected == "WEATHER":
       if arryStatus == 0:
        data['lcd']['lcd_1'].update({ "disp":["bme280","pressure"], "add": "Pressure:"})
        data['lcd']['lcd_2'].update({ "disp":["bme280","humidity"], "add": "Humidity:"})

       if arryStatus == 1:
        data['lcd']['lcd_1'].update({ "disp":["bme280","temperature"], "add": "BoatTmp:"})
        data['lcd']['lcd_2'].update({ "disp":["ds180","OutTemp"], "add": "OutTmp :"})


       if arryStatus == 2:
        data['lcd']['lcd_1'].update({ "disp":["ds180","WTemp"],   "add": "WaterTmp:"})
        data['lcd']['lcd_2'].update({ "disp":["None","None"],   "add": "None"})
       pub.dispatch(data)
       blockListener = False
       return


       pub.dispatch(data)

     if selected == "MAINSELECTION":
         data['lcd']['lcd_2']["disp"] =  ["None", "None"] 
         if arryStatus == 0:
            blockListener = True
            data['lcd']['lcd_1']["disp"] =  ["mainList", "GPS"] 
            

         if arryStatus == 1:
            blockListener = True
            data['lcd']['lcd_1']["disp"] =  ["mainList", "WEATHER"] 
            

         if arryStatus == 2 :
            blockListener = True
            data['lcd']['lcd_1']["disp"] =  ["mainList", "PI"]
        
         if arryStatus == 3:

            data['lcd']['lcd_1']["disp"] =  ["date", "date"]
            data['lcd']['lcd_2']["disp"] =  ["date", "time"] 

            blockListener = False
         pub.dispatch(data)
         return
     if selected == "PI":
         data['lcd']['lcd_1']["add"] = "None"
         data['lcd']['lcd_2']["disp"] =  ["None", "None"]
         data['lcd']['lcd_2']["add"] = "None"
         if arryStatus == 0:
            blockListerner = True
            data['lcd']['lcd_1']["disp"] =  ["pi", "shutdown"] 

         if arryStatus == 1:
            blockListerner = True
            data['lcd']['lcd_1']["disp"] =  ["pi", "restart_py"] 

         if arryStatus == 2:
            blockListerner = True
            data['lcd']['lcd_1']["disp"] =  ["pi", "reboot"]
         if arryStatus == 3:
            blockListerner = True
            data['lcd']['lcd_1']["disp"] =  ["pi", "return"] 
         pub.dispatch(data)

     return

# Listener on Pressed
def switchPressed():
    print("PRESSED")
    global rotary,observer,mainList,arryStatus,gps, selected ,data, maxLen, blockListener
    ## Move this line to observer
    if selected == "GPS" or selected == "WEATHER":
     selected = "MAINSELECTION"
     blockListener = True    

     data['lcd']['lcd_1'].update({"disp":["mainList","GPS"], "add": "None"})
     data['lcd']['lcd_2'].update({"disp":["None","None"], "add": "None"})

     pub.dispatch(data)
     arryStatus = 0
     maxLen=4
     return

        
   ## GPS
    if data['lcd']['lcd_1']["disp"] ==  ["mainList", "GPS"]:
     selected = "GPS"
     data['lcd']['lcd_1']["disp"] =  ["gps","latitude"]
     data['lcd']['lcd_2']["disp"] =  ["gps","longitude"]
     pub.dispatch(data)
     blockListener = False
     maxLen = 2
     arryStatus = 0
     return
   # Weather
    if data['lcd']['lcd_1']["disp"] ==  ["mainList", "WEATHER"] :
     selected = "WEATHER"
     data['lcd']['lcd_1'].update({ "disp":["bme280","pressure"], "add": "Pressure:"})
     data['lcd']['lcd_2'].update({ "disp":["bme280","humidity"], "add": "humidity:"})
     pub.dispatch(data)
     blockListener = False
     maxLen = 2
     arryStatus = 0
     return

   # PI
    if data['lcd']['lcd_1']["disp"] ==  ["mainList", "PI"] :
     selected = "PI"
     data['lcd']['lcd_1'].update({ "disp":["pi","shutdown"], "add": "None"})
     data['lcd']['lcd_2']["disp"] =  ["None", "None"]
     data['lcd']['lcd_2']["add"] = "None"
     pub.dispatch(data)
     blockListener = False
     maxLen = 3
     arryStatus = 0
     return
   # Pi
    if data['lcd']['lcd_1']["disp"] ==  ["pi", "reboot"] :
       data['pi']['reboot'] = "Rebooting.."
       startAisDecoder.killAllOnAddress()
       toggleRelais("off")

       pub.dispatch(data)
       pi_system.reboot()

    if data['lcd']['lcd_1']["disp"] ==  ["pi", "shutdown"] :
       data['pi']['shutdown'] = "Shutdown.."
       pub.dispatch(data)
       startAisDecoder.killAllOnAddress()
       toggleRelais("off")

       pi_system.shutdown()
    if data['lcd']['lcd_1']["disp"] ==  ["pi", "restart_py"] :
       data['pi']['shutdown'] = "RestartPy.."
       pub.dispatch(data)
       startAisDecoder.killAllOnAddress()
       toggleRelais("off")

       pi_system.restartPy()
    if data['lcd']['lcd_1']["disp"] ==  ["pi", "return"] :
       selected = "MAINSELECTION"
       data['lcd']['lcd_1'].update({"disp":["mainList","GPS"], "add": "None"})
       data['lcd']['lcd_2'].update({"disp":["None","None"], "add": "None"})
       arryStatus = 0
       maxLen=4
       pub.dispatch(data)

    return
 
# Init KyoButton
CLOCKPIN = 5
DATAPIN = 6
SWITCHPIN = 13
GPIO.setmode(GPIO.BCM)
ky040 = KY040.KY040(CLOCKPIN, DATAPIN, SWITCHPIN, rotaryChange, switchPressed)
ky040.start()

try:
    while True:
        sleep(0.1)
finally:
    ky040.stop()
    GPIO.cleanup()
