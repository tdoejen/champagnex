const LCD = require('raspberrypi-liquid-crystal');
const lcd = new LCD( 1, 0x27, 16, 2 );
const Rotary = require('raspberrypi-rotary-encoder');
const fetch = require('node-fetch');

// WARNING ! This is WIRINGPI pin numerotation !! please see https://fr.pinout.xyz/pinout/wiringpi#*
const pinClk = 21;
const pinDt = 22;
const pinSwitch = 23;  // Optional switch

const rotary = new Rotary(pinClk, pinDt, pinSwitch);
try {
    lcd.beginSync();
  } catch (e) {
    console.log(e);
  }

// State Dispatch
const Rehoard = require("rehoard")
const rehoard = new Rehoard();

rehoard.create("buildMsgArray", {});
rehoard.create("updateLcd", []);

lcd.clearSync();

let startStringUpper = 'Welcome on Board of' 
let startStringUnder = ' ChampagneXpress' 

lcd.setCursorSync(0,0);

lcd.printSync( startStringUpper );
lcd.setCursorSync(0,1);

lcd.printSync( startStringUnder );

let  upperLineOld=startStringUpper
let  underLineOld= startStringUnder

const buildMsgArrayListener = rehoard.subscribe("buildMsgArray",  (msgPos) => {
    let upperLine = menuPoints[msgPos.menuState].items[msgPos.counter].upperLine
    if( menuPoints[msgPos.menuState].items[msgPos.counter].upperLineData){
        let dataPath = menuPoints[msgPos.menuState].items[msgPos.counter].upperLineData
        data = dataObj[dataPath[0]][dataPath[1]]
        upperLine = upperLine + data
    }
    let underLine = menuPoints[msgPos.menuState].items[msgPos.counter].underLine
    if( menuPoints[msgPos.menuState].items[msgPos.counter].underLineData){
      let dataPath = menuPoints[msgPos.menuState].items[msgPos.counter].underLineData

      data = dataObj[dataPath[0]][dataPath[1]]
      underLine = underLine + data
  }

  rehoard.dispatch("updateLcd", [upperLine,underLine]);

}); 
const updateLcdListener = rehoard.subscribe("updateLcd",  (lineArry) => {
   // lcd.clearSync();
    if(lineArry[0] !== upperLineOld){
      
      diffArry = firstDiff(lineArry[0].padEnd(16), upperLineOld.padEnd(16))
      upperLineOld= lineArry[0]
      for(const rec of diffArry){
        lcd.setCursorSync(rec,0);
        let print =  lineArry[0][rec]
        if(!print){
          print = " "
        }
        lcd.printSync( print);
      }


    }
    if(lineArry[1] !== underLineOld){
      diffArry = firstDiff(lineArry[1].padEnd(16), underLineOld.padEnd(16))
      underLineOld= lineArry[1]
      for(const rec of diffArry){
        lcd.setCursorSync(rec,1);
        let print2 =  lineArry[1][rec]
        if(!print2){
          print2 = " "
        }
        lcd.printSync( print2);
      }

    }
});




function firstDiff(a, b) {
  diff = []
  var i = a.split('').findIndex(function(c, i) {
      if(c != b[i]) {diff.push(i)}});
      return diff
}




let menuPoints = {
    mainMenu:    {counter_state:0, counter_length: 3, pressAction:["navMenu", "weatherMenu","piMenu",false], items: [{upperLine: "   NAVIGATION  ", underLine: ""}, {upperLine: "    WEATHER    ", underLine:  ""}, {upperLine: "       PI       ", underLine:  ""}, {upperLine: "      TIME     ", underLine:  ""}]},
    navMenu:     {counter_state:0, counter_length: 3, pressAction:["mainMenu","mainMenu","mainMenu"] ,items: [{upperLine: "LAT: ",upperLineData: ["navigation","lat"], underLine: "LON: ", underLineData: ["navigation","lon"] },{upperLine: "COG: " ,upperLineData: ["navigation","cog"],underLine: "MAG: ",  underLineData: ["navigation","headingMagnetic"]},{upperLine: "SOG: ",upperLineData: ["navigation","sog"], underLine: "FIX: ",underLineData: ["navigation","gnss_quality"]},{upperLine: "Satellites: ",upperLineData: ["navigation","satellites"], underLine: "Altitude: ",underLineData: ["navigation","antennaAltitude"]}]},
    weatherMenu:     {counter_state:0, counter_length: 1, pressAction:["mainMenu","mainMenu"] ,items: [{upperLine: "PRESSURE: ",upperLineData: ["environment","pressure"], underLine: "TEMP: ",underLineData: ["environment","temperature"] },{upperLine: "HUMIDITY: ", underLine: "",upperLineData: ["environment","relativeHumidity"]}]},
    shipMenu:       {counter_state:0, counter_length: 0, pressAction:["mainMenu"] ,items: [{upperLine: "MMSI ",upperLineData: ["navigation","lat"], underLine: "CALL_SIGN: ", }]},
    piMenu:      {counter_state:0, counter_length: 2, pressAction:["mainMenu","mainMenu","mainMenu"], items:[{upperLine: "    SHUTDOWN    ", underLine: " " },{upperLine: "     REBOOT    ", underLine: " "},{upperLine: "    MAINMENU    ", underLine: " "}]}

  }

let menuState = "mainMenu"


const getCounter = (menuState, delta)=> {
    delta = delta*-1
    let menueMaxRotateLength = menuPoints[menuState].counter_length
    menuPoints[menuState].counter_state = menuPoints[menuState].counter_state + delta
    if ( menuPoints[menuState].counter_state > menueMaxRotateLength) {
        menuPoints[menuState].counter_state = 0
    }
    if ( menuPoints[menuState].counter_state < 0) {
        menuPoints[menuState].counter_state = menueMaxRotateLength
    }
    return  menuPoints[menuState].counter_state
}

let dataObj = {
  navigation:
      {"lat":null,"lon": null,"headingMagnetic": null, "magneticVariation": null,"cog": null, "sog":null, "gnss_quality": null, satellites: null },
  environment: {}
  }

  const posDecimalToDegMin = (value,dir)=>{
    let hemisphere
    if(dir==="lat"){
      if (value > 0 ){
        hemisphere = "N"
      }else{
        hemisphere = "S"
      }
    }
    if(dir==="lon"){
      if (value > 0 ){
        hemisphere = "E"
      }else{
        hemisphere = "W"
      }
    }
    value = Math.abs(value)
    console.log("Value",value)
    let posDecimalToDegMin = null
    if(value !== null){
      const degreeValue  = Math.trunc( value );
      const decimalValue =  value - Math.floor(value)
      const minValue = Math.trunc(decimalValue*60)
      const secValue =  Math.trunc((decimalValue - minValue/60)*3600)
      let minValueString = (minValue).toString()
      let secValueString =  Math.trunc((decimalValue - minValue/60)*3600).toString()
      let degreeValueString  = Math.trunc( value );
      if(degreeValueString.length === 1){
        degreeValueString = "0" + degreeValueString
      }
      if(minValueString.length === 1){
        minValueString = "0" + minValueString
      }
      if(secValueString.length === 1){
        secValueString = "0" + secValueString
      }

       posDecimalToDegMin = degreeValueString + String.fromCharCode(223) + minValueString  + String.fromCharCode(039)  + secValueString + String.fromCharCode(039)+ String.fromCharCode(039)  + hemisphere  }
    return posDecimalToDegMin
  }

setInterval(async ()=>{
    const signalKResult = await fetch('http://openplotter:3000/signalk/v1/api/vessels/urn:mrn:imo:mmsi:211428440')

    if(signalKResult.status === 200){
      const body =  await signalKResult.json()
      const navigation = body.navigation
      dataObj.navigation.lat = posDecimalToDegMin(navigation.position.value.latitude,"lat") 
      dataObj.navigation.lon =  posDecimalToDegMin(navigation.position.value.longitude,"lon") 
      dataObj.navigation.cog = Math.trunc(navigation.courseOverGroundTrue.value * 180/Math.PI) + String.fromCharCode(223)
      dataObj.navigation.headingMagnetic =  Math.trunc((navigation.headingMagnetic.value + navigation.magneticVariation.value) * 180/Math.PI) +  String.fromCharCode(223)

      dataObj.navigation.sog = Number.parseFloat(navigation.speedOverGround.value * 1,944).toFixed(1)
      dataObj.navigation.magneticVariation = navigation.magneticVariation.value 
      dataObj.navigation.gnss_quality = navigation.gnss.methodQuality.value
      dataObj.navigation.satellites = navigation.gnss.satellites.value
      dataObj.navigation.antennaAltitude = navigation.gnss.antennaAltitude.value
      const environment = body.environment.outside
      dataObj.environment.pressure =Math.trunc( environment.pressure.value * 0.01)
      dataObj.environment.temperature =  Number.parseFloat(environment.temperature.value - 273,15,2).toFixed(1)
      rehoard.dispatch("buildMsgArray", {menuState: menuState, counter:  menuPoints[menuState].counter_state});
      const environment_inside = body.environment.inside
      dataObj.environment.relativeHumidity = environment_inside.relativeHumidity.value
    }
   }, 
    1000);

rotary.on("rotate", (delta) => {

   let counter =  getCounter(menuState,delta)
   menuPoints[menuState].counter_state = counter
   rehoard.dispatch("buildMsgArray", {menuState: menuState, counter: counter});
});
  
  rotary.on("pressed", () => {
    let counterState =  menuPoints[menuState].counter_state 
    let pressAction = menuPoints[menuState].pressAction[counterState]
    if(typeof pressAction === "string" ){
      if(pressAction){
        menuState = pressAction
        let counter =  getCounter(menuState,0)
        menuPoints[menuState].counter_state = counter
        rehoard.dispatch("buildMsgArray", {menuState: menuState, counter: counter});
      }
    }

    if(typeof pressAction === "function" ){
    console.log("ts", typeof pressAction)
    pressAction('a string')
  }});
  
  rotary.on("released", () => {
    console.log("Rotary switch released");
  });
  

