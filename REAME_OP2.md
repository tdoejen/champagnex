### Install with new Openplotter 2

# Enable ssh 
Enable ssh over 
Einstellungen-Raspberry-Pi-Konfiguration

# Install SSH-FS to run from VSCODE

sudo apt install sshfs

# Check DSIK SPACE
df -h

# Pypilot Flask WS-IO has some Errors
Install correct version
https://stackoverflow.com/questions/65144726/app-engine-flask-socketio-server-cors-allowed-origins-header-is-missing
Version running
 flask-socketio-4.3.2 python-engineio-3.14.2 python-socketio-4.6.1


# Install Postgresql

sudo apt install postgresql libpq-dev postgresql-client postgis

postgresql-client-common -y

sudo su postgres
createuser pi -P --interactive

CREATE DATABASE champagnex;
CREATE EXTENSION postgis;

user = "pi",
password = "D!3W.A=champagnex",
database = "champagnex"

 # Python Modules for LCD Code
  pip3 install threaded
  pip3 install pytz
  pip3 install pscopg2